package com.demandbridge.qapipellineutil.controller;

import com.demandbridge.qapipellineutil.service.BitbucketService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/1.0/bitbucket")
public class BitbucketController {

    private final BitbucketService bitbucketService;

    public BitbucketController(BitbucketService bitbucketService) {
        this.bitbucketService = bitbucketService;
    }

    // http://localhost:8080/api/1.0/bitbucket/repositories/dba-ui-test/pipelines
    @GetMapping("/repositories/{repositoryName}/pipelines")
    public ResponseEntity<?> getPipelinesForRepo(@PathVariable String repositoryName) {
        return bitbucketService.getRepoPipelines(repositoryName);
    }

    @PostMapping("/repositories/{repositoryName}/pipelines")
    public ResponseEntity<String> triggerPipelineForRepo(@PathVariable String repositoryName) {
        return bitbucketService.triggerPipelineForRepo(repositoryName);
    }
}

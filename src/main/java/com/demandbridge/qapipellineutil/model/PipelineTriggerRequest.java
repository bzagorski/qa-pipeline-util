package com.demandbridge.qapipellineutil.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class PipelineTriggerRequest {

    private Target target;

    public PipelineTriggerRequest() {
        this.target = new Target("branch", "pipeline_ref_target", "master");
    }

    public PipelineTriggerRequest(String branchName) {
        this.target = new Target("branch", "pipeline_ref_target", branchName);
    }

    public Target getTarget() {
        return target;
    }

    public class Target {

        @JsonProperty("ref_type")
        private String refType;

        private String type;

        @JsonProperty("ref_name")
        private String refName;

        public Target(String refType, String type, String refName) {
            this.refType = refType;
            this.type = type;
            this.refName = refName;
        }

        public String getRefType() {
            return refType;
        }

        public void setRefType(String refType) {
            this.refType = refType;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getRefName() {
            return refName;
        }

        public void setRefName(String refName) {
            this.refName = refName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Target target = (Target) o;
            return Objects.equals(refType, target.refType) && Objects.equals(type, target.type) && Objects.equals(refName, target.refName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(refType, type, refName);
        }
    }
}

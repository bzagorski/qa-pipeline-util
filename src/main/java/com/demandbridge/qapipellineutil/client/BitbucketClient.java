package com.demandbridge.qapipellineutil.client;

import org.springframework.http.ResponseEntity;

public interface BitbucketClient {

    ResponseEntity<String> getPipelinesForRepo(String repoName);

    ResponseEntity<String> triggerPipelineForRepo(String repoName);
}

package com.demandbridge.qapipellineutil.client;

import com.demandbridge.qapipellineutil.model.PipelineTriggerRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

@Component("httpClient")
public class BitbucketHttpClient implements BitbucketClient {

    @Value("${bitbucket.username:username}")
    private String username;

    @Value("${bitbucket.password:password}")
    private String password;

    private static final String BITBUCKET_BASE_URI = "https://api.bitbucket.org/2.0/repositories/demandbridge";

    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .authenticator(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password.toCharArray());
                }
            })
            .connectTimeout(Duration.ofSeconds(10))
            .build();

    private final ObjectMapper objectMapper;

    @Autowired
    public BitbucketHttpClient(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public ResponseEntity<String> getPipelinesForRepo(String repoName) {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(String.format("%s/%s/pipelines/", BITBUCKET_BASE_URI, repoName)))
                .header(HttpHeaders.CONTENT_TYPE, "application/json")
                .build();

        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            return ResponseEntity.ok(objectMapper.writeValueAsString(response.body()));
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(String.format("Error occurred obtaining pipelines for repository: %s", repoName));
        }
    }

    @Override
    public ResponseEntity<String> triggerPipelineForRepo(String repoName) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(new PipelineTriggerRequest(repoName))))
                    .uri(URI.create(String.format("%s/%s/pipelines/", BITBUCKET_BASE_URI, repoName)))
                    .setHeader("Content-Type", "application/json")
                    .build();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 200) {
                return ResponseEntity.ok("Pipeline for repository " + repoName + " was successfully triggered");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred triggering pipeline for repository " + repoName);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Invalid pipeline request");
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred triggering pipeline for repository ");
        }
    }
}

package com.demandbridge.qapipellineutil.client;

import com.demandbridge.qapipellineutil.model.PipelineTriggerRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

@Primary
@Component("restTemplate")
public class BitbucketRestTemplateClient implements BitbucketClient {

    private static final String BITBUCKET_BASE_URL = "https://api.bitbucket.org/2.0/repositories/demandbridge";

    private final RestTemplate restTemplate;

    private final ObjectMapper objectMapper;

    public BitbucketRestTemplateClient(
            RestTemplateBuilder restTemplateBuilder,
            ObjectMapper objectMapper,
            @Value("${bitbucket.username:username}") String username,
            @Value("${bitbucket.password:password}") String password
    ) {
        this.restTemplate = restTemplateBuilder
                .basicAuthentication(username, password)
                .defaultHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
        this.objectMapper = objectMapper;
    }

    @Override
    public ResponseEntity<String> getPipelinesForRepo(String repoName) {
        return restTemplate.getForEntity(
                URI.create(String.format("%s/%s/pipelines/", BITBUCKET_BASE_URL, repoName)),
                String.class
        );
    }

    @Override
    public ResponseEntity<String> triggerPipelineForRepo(String repoName) {
        try {
            return restTemplate.postForEntity(
                    String.format("%s/%s/pipelines/", BITBUCKET_BASE_URL, repoName),
                    objectMapper.writeValueAsString(new PipelineTriggerRequest()),
                    String.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Invalid PipelineTriggerRequest");
        }
    }
}

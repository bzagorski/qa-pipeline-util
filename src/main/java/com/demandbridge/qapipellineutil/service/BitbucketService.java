package com.demandbridge.qapipellineutil.service;

import com.demandbridge.qapipellineutil.client.BitbucketClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class BitbucketService {

    private final BitbucketClient bitbucketClient;

    public BitbucketService(BitbucketClient bitbucketClient) {
        this.bitbucketClient = bitbucketClient;
    }

    public ResponseEntity<String> getRepoPipelines(String repoName) {
        return bitbucketClient.getPipelinesForRepo(repoName);
    }

    public ResponseEntity<String> triggerPipelineForRepo(String repoName) {
        ResponseEntity<String> response = bitbucketClient.triggerPipelineForRepo(repoName);
        if (response.getStatusCode().is2xxSuccessful()) {
            return ResponseEntity.ok("Pipeline triggered for repository: " + repoName);
        } else {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Error occurred attempting to trigger pipeline for repository: " + repoName);
        }
    }
}

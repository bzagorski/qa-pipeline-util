package com.demandbridge.qapipellineutil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class QaPipelineUtilApplication {

	public static void main(String[] args) {
		SpringApplication.run(QaPipelineUtilApplication.class, args);
	}

}

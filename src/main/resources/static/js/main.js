const repositorySelect = document.getElementById('repositories')
const pipelineButton = document.getElementById('pipeline-btn')

pipelineButton.addEventListener('click', function (event) {
    event.preventDefault()
    const repositoryName = repositorySelect.value
    const url = `/api/1.0/bitbucket/repositories/${repositoryName}/pipelines`
    if (!repositoryName) {
        alert("Please select a repository")
        return
    }
    if (confirm(`Are you sure you want to trigger the pipeline for ${repositoryName}`)) {
        fetch(url, {method: 'POST'})
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                console.log(response);
                M.toast({html: `${repositoryName} pipeline successfully triggered`, classes: 'teal'})
            }).catch(error => {
            console.log(error)
            M.toast({html: `Error triggering pipeline for ${repositoryName}`, classes: 'red'})
        })
    }
})
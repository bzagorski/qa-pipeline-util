# QA Pipeline Util
* Simple utility app for triggering qa pipelines hosted on [Bitbucket Pipelines](https://bitbucket.org/demandbridge/workspace/projects/QA)
* Initial MVP will consist of a landing page that allows these pipelines to be triggered

### Installation
* Java 11+
* Maven
* Set environment variables for BITBUCKET_USERNAME and BITBUCKET_PASSWORD

### Road Map
* Unit tests
* RestAssured API tests (mocked)
* UI tests
* Scheduled tasks for pipeline executions